package com.github.kvnxiao.kommandant.discord.core.command

/**
 * An annotation class used in defining permissions for a Discord command when creating Discord commands through
 * annotation processing.
 */
@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.FUNCTION)
annotation class PermissionLevel(
        /**
         * @see[PermissionDefaults.REQUIRE_MENTION]
         */
        val reqMention: Boolean = PermissionDefaults.REQUIRE_MENTION,
        /**
         * @see[PermissionDefaults.ALLOW_DM]
         */
        val allowDm: Boolean = PermissionDefaults.ALLOW_DM,
        /**
         * @see[PermissionDefaults.FORCE_DM_REPLY]
         */
        val forceDmReply: Boolean = PermissionDefaults.FORCE_DM_REPLY,
        /**
         * @see[PermissionDefaults.REMOVE_CALL_MSG]
         */
        val removeCallMsg: Boolean = PermissionDefaults.REMOVE_CALL_MSG,
        /**
         * @see[PermissionDefaults.DEFAULT_PERMS_RW]
         */
        val level: Array<Permissions> = arrayOf(Permissions.MESSAGE_READ, Permissions.MESSAGE_WRITE),
        /**
         * @see[PermissionDefaults.RATE_LIMIT_PERIOD_MS]
         */
        val rateLimitPeriodMs: Long = PermissionDefaults.RATE_LIMIT_PERIOD_MS,
        /**
         * @see[PermissionDefaults.TOKENS_PER_PERIOD]
         */
        val tokensPerPeriod: Long = PermissionDefaults.TOKENS_PER_PERIOD,
        /**
         * see[PermissionDefaults.RATE_LIMIT_ON_GUILD]
         */
        val rateLimitOnGuild: Boolean = PermissionDefaults.RATE_LIMIT_ON_GUILD
)