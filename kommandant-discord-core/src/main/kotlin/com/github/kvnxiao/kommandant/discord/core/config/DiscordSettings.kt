package com.github.kvnxiao.kommandant.discord.core.config

import com.fasterxml.jackson.annotation.JsonIgnore

/**
 * Discord related settings such as custom mentions that can be used in prefixes.
 */
data class DiscordSettings(val includeMentionPrefix: Boolean = true,
                           val customMentions: MutableSet<String> = mutableSetOf()) {

    @JsonIgnore
    fun getMentionsForRegex(): String = customMentions.joinToString("|")

    @JsonIgnore
    fun isMentionsEmpty(): Boolean = customMentions.isEmpty()

}