package com.github.kvnxiao.kommandant.discord.core.config

/**
 * A data class containing configuration properties such as thread pool size and the location of the config folder.
 *
 * @property[threadPoolSize] The size of the thread pool for executing commands.
 * @property[loadExternalCommands] Whether Kommandant should load external .jar commands. Defaults to true.
 * @property[cmdConfigFolder] The configuration folder where command property JSON files are stored.
 * @property[cmdJarFolder] The jar folder where external command jars are to be loaded from.
 */
data class StartupConfig(val threadPoolSize: Int = 2,
                         val loadExternalCommands: Boolean = true,
                         val cmdConfigFolder: String = "kommandant/props/",
                         val cmdJarFolder: String = "kommandant/",
                         val settingsFolder: String = "kommandant/settings/")