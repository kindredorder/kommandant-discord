package com.github.kvnxiao.kommandant.discord.core.command

import com.github.kvnxiao.kommandant.command.CommandProperties
import com.github.kvnxiao.kommandant.command.ICommand

/**
 * An abstract class representing a command with properties and permissions.
 *
 * @property[props] The command properties describing this command.
 * @property[perms] The permission properties describing how this command should be called.
 */
abstract class ICommandDiscord<T>(props: CommandProperties, var perms: PermissionProperties) : ICommand<T>(props) {

    val rateLimitManager = RateLimitManager()

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ICommandDiscord<*>) return false

        if (props != other.props) return false
        if (subCommands != other.subCommands) return false
        if (perms != other.perms) return false

        return true
    }

    override fun hashCode(): Int {
        var result = props.hashCode()
        result = 31 * result + perms.hashCode()
        result = 31 * result + subCommands.hashCode()
        return result
    }

}