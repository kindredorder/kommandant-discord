package com.github.kvnxiao.kommandant.discord.core.command

import com.github.bucket4j.Bucket
import com.github.bucket4j.Bucket4j
import java.time.Duration

/**
 * A rate limit manager for a specific command
 */
open class RateLimitManager {

    /**
     * Map of guild snowflake id to user id : bucket
     */
    val guildManager: MutableMap<String, MutableMap<String, Bucket>> = mutableMapOf()
    /**
     * Map of user id to bucket for private channels
     */
    val privateManager: MutableMap<String, Bucket> = mutableMapOf()
    /**
     * Map of guild id to bucket for rate limiting commands on a guild-global scale
     */
    val guildBucket: MutableMap<String, Bucket> = mutableMapOf()

    fun getOrCreateUserMap(guildId: String): MutableMap<String, Bucket> {
        if (!guildManager.contains(guildId)) {
            guildManager[guildId] = mutableMapOf()
        }
        return guildManager[guildId]!!
    }

    fun getOrCreateBucket(mutableUserMap: MutableMap<String, Bucket>, id: String, perms: PermissionProperties): Bucket {
        if (!mutableUserMap.contains(id)) {
            val bucket = Bucket4j.builder().withLimitedBandwidth(perms.tokensPerPeriod, Duration.ofMillis(perms.rateLimitPeriodMs)).build()
            mutableUserMap[id] = bucket
        }
        return mutableUserMap[id]!!
    }

    fun isNotRateLimited(guildId: String, userId: String, perms: PermissionProperties): Boolean = if (!perms.rateLimitOnGuild || guildId.equals("")) getOrCreateBucket(getOrCreateUserMap(guildId), userId, perms).tryConsumeSingleToken() else getOrCreateBucket(guildBucket, guildId, perms).tryConsumeSingleToken()

    fun isNotRateLimited(userId: String, perms: PermissionProperties): Boolean = getOrCreateBucket(privateManager, userId, perms).tryConsumeSingleToken()

}
