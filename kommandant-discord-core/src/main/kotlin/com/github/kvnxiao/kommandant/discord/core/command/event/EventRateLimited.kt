package com.github.kvnxiao.kommandant.discord.core.command.event

import com.github.kvnxiao.kommandant.command.CommandContext

/**
 * Interface for when a command is rated limited for the current user.
 */
interface EventRateLimited {
    /**
     * Method called as a result of a user attempting to issuing the command when rate limited by it.
     */
    fun onRateLimited(context: CommandContext, isDm: Boolean, event: Any)
}