package com.github.kvnxiao.kommandant.discord.core.command

import java.util.*

/**
 * A data class containing the permission properties of the command.
 *
 * @property[reqMention] Whether the command requires a bot mention in order to activate.
 * @property[allowDm] Whether the command is allowed to be activated through a direct message to the bot.
 * @property[forceDmReply] Whether the command is forced to reply in a private chat or not.
 * @property[removeCallMsg] Whether the calling message is removed after activating the command.
 * @property[level] The permission level required to call this command.
 */
data class PermissionProperties(
        var reqMention: Boolean = PermissionDefaults.REQUIRE_MENTION,
        var allowDm: Boolean = PermissionDefaults.ALLOW_DM,
        var forceDmReply: Boolean = PermissionDefaults.FORCE_DM_REPLY,
        var removeCallMsg: Boolean = PermissionDefaults.REMOVE_CALL_MSG,
        var level: EnumSet<Permissions> = PermissionDefaults.DEFAULT_PERMS_RW,
        var rateLimitPeriodMs: Long = PermissionDefaults.RATE_LIMIT_PERIOD_MS,
        var tokensPerPeriod: Long = PermissionDefaults.TOKENS_PER_PERIOD,
        var rateLimitOnGuild: Boolean = PermissionDefaults.RATE_LIMIT_ON_GUILD
)