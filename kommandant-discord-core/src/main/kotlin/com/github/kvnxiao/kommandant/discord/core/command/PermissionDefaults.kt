package com.github.kvnxiao.kommandant.discord.core.command

import java.util.*

/**
 * An object class containing the default command permission settings
 */
object PermissionDefaults {

    /**
     * The setting for whether the command requires a bot mention to activate, defaults to false.
     */
    const val REQUIRE_MENTION = false
    /**
     * The setting for whether the command can be activated through direct message to the bot, defaults to false.
     */
    const val ALLOW_DM = false
    /**
     * The setting for whether the command replies are forced as a direct message, defaults to false.
     */
    const val FORCE_DM_REPLY = false
    /**
     * The setting for whether the message used to activate the command should be removed post-activation, defaults to false.
     */
    const val REMOVE_CALL_MSG = false
    /**
     * The time period for rate limiting a command in milliseconds, defaults to 1000 ms or 1 second.
     */
    const val RATE_LIMIT_PERIOD_MS: Long = 1000
    /**
     * The number of valid tokens or calls per rate limit period for a command, defaults to 3 calls per second.
     */
    const val TOKENS_PER_PERIOD: Long = 3
    /**
     * The setting for whether rate limiting is per guild or per user, defaults to false meaning per user.
     */
    const val RATE_LIMIT_ON_GUILD = false

    /**
     * The default permission level set required to execute the command, defined as a Discord user having the
     * READ and WRITE permissions.
     */
    @JvmField
    val DEFAULT_PERMS_RW: EnumSet<Permissions> = EnumSet.of(Permissions.MESSAGE_READ, Permissions.MESSAGE_WRITE)

}