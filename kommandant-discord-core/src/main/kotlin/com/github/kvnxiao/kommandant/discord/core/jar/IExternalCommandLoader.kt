package com.github.kvnxiao.kommandant.discord.core.jar

/**
 * An interface for defining a method to load external commands from the classpath on runtime.
 */
interface IExternalCommandLoader {
    /**
     * This method should load external commands from the classpath on runtime.
     */
    fun loadExternalCommands()
}