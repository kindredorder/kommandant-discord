package com.github.kvnxiao.kommandant.discord.core

import com.fasterxml.jackson.databind.JsonNode
import com.fasterxml.jackson.databind.node.ObjectNode
import com.fasterxml.jackson.module.kotlin.readValue
import com.fasterxml.jackson.module.kotlin.treeToValue
import com.github.kvnxiao.kommandant.ICommandBank
import com.github.kvnxiao.kommandant.ICommandExecutor
import com.github.kvnxiao.kommandant.ICommandParser
import com.github.kvnxiao.kommandant.KommandantConfigurable
import com.github.kvnxiao.kommandant.command.CommandProperties
import com.github.kvnxiao.kommandant.command.CommandResult
import com.github.kvnxiao.kommandant.command.ICommand
import com.github.kvnxiao.kommandant.configurable.CommandConfig
import com.github.kvnxiao.kommandant.configurable.CommandConfig.CONFIG_FOLDER
import com.github.kvnxiao.kommandant.configurable.CommandConfig.OBJECT_MAPPER
import com.github.kvnxiao.kommandant.discord.core.command.ICommandDiscord
import com.github.kvnxiao.kommandant.discord.core.command.PermissionProperties
import com.github.kvnxiao.kommandant.discord.core.config.DiscordSettings
import com.github.kvnxiao.kommandant.discord.core.config.StartupConfig
import com.github.kvnxiao.kommandant.discord.core.utility.ThreadFactory
import com.github.kvnxiao.kommandant.impl.CommandBank
import com.github.kvnxiao.kommandant.impl.CommandExecutor
import com.github.kvnxiao.kommandant.impl.CommandParser
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.Future

/**
 * An extension of the KommandantConfigurable class which allows command enabling, disabling, and property saving.
 * Additional functionality here includes a local thread pool for executing commands asynchronously using Futures
 * and saving permission properties to JSON.
 *
 * @property[enableConfigReadWrite] Whether commands should have their properties loaded from and written to JSON.
 * @property[cmdBank] The command registry, an implementation of [ICommandBank].
 * @property[cmdExecutor] The command executor, an implementation of [ICommandExecutor].
 * @property[cmdParser] The command parser, an impementation of [ICommandParser].
 * @constructor Default constructor which uses default implementations of the registry, parser, and executor.
 */
open class KommandantDiscord(
        enableConfigReadWrite: Boolean = false,
        cmdBank: ICommandBank = CommandBank(),
        cmdExecutor: ICommandExecutor = CommandExecutor(),
        cmdParser: ICommandParser = CommandParser()) : KommandantConfigurable(enableConfigReadWrite, cmdBank, cmdExecutor, cmdParser) {

    /**
     * Container object holding default constant values
     */
    companion object {
        /**
         * Core configurations for Kommandant Discord implementations will be found in a file called "konfig.json"
         */
        const val CONFIG_NAME = "konfig.json"
        /**
         * JSON object field for command properties will be named as "properties"
         */
        const val JSON_PROPERTIES_NAME = "properties"
        /**
         * JSON object field for permissions will be named as "permissions"
         */
        const val JSON_PERMISSIONS_NAME = "permissions"
        /**
         * Discord bot related settings will be found in a file called "settings.json"
         */
        const val SETTINGS_NAME = "settings.json"
        /**
         * Use '%m' in the prefix of a command for activating custom mention prefixes.
         */
        const val CUSTOM_MENTION_KEY = "%m"
    }

    /**
     * The thread pool executor service for executing commands in another thread.
     */
    protected val threadPool: ExecutorService

    /**
     * The configuration settings.
     *
     * @see[StartupConfig]
     */
    protected val config: StartupConfig

    /**
     * The discord related settings for Kommandant.
     *
     * @see[DiscordSettings]
     */
    protected val discordSettings: DiscordSettings

    init {
        // Load core configuration settings
        val configFile = Paths.get(CONFIG_NAME)
        if (Files.exists(configFile)) {
            val configJson = Files.readAllBytes(configFile)
            config = OBJECT_MAPPER.readValue<StartupConfig>(configJson)
        } else {
            config = StartupConfig()
            val configJson = OBJECT_MAPPER.writeValueAsBytes(config)
            Files.write(configFile, configJson)
        }
        threadPool = Executors.newFixedThreadPool(config.threadPoolSize, ThreadFactory())
        CONFIG_FOLDER = config.cmdConfigFolder

        // Load discord specific settings
        val settingsFile = Paths.get(config.settingsFolder + SETTINGS_NAME)
        if (Files.exists(settingsFile)) {
            val settingsJson = Files.readAllBytes(settingsFile)
            discordSettings = OBJECT_MAPPER.readValue<DiscordSettings>(settingsJson)
        } else {
            discordSettings = DiscordSettings()
            val settingsJson = OBJECT_MAPPER.writeValueAsBytes(discordSettings)
            Files.createDirectories(settingsFile.parent)
            Files.createFile(settingsFile)
            Files.write(settingsFile, settingsJson)
        }
    }

    /**
     * Processes a string input with any additional variables for asynchronous command execution using Futures.
     *
     * @param[input] The string input to parse into valid context and command.
     * @param[opt] A nullable vararg of optional, extra input variables.
     * @return[Future] The future result after attempting execution of the command asynchronously in another thread.
     */
    open fun <T> processAsync(input: String, vararg opt: Any?): Future<CommandResult<T>> {
        return threadPool.submit<CommandResult<T>> { process(input, *opt) }
    }

    /**
     * Adds a normal command to the command registry, checking if it is a Discord command and if so, optionally loads
     * and saves command properties and permissions externally.
     *
     * @param[command] The command to add to the bank.
     * @return[Boolean] Whether the command was successfully added to the bank or not.
     */
    override fun addCommand(command: ICommand<*>): Boolean = if (command is ICommandDiscord<*>) this.addCommand(command) else super.addCommand(command)

    /**
     * Adds an [ICommandDiscord] command to the command registry, which optionally loads and saves command properties
     * and permissions externally.
     *
     * @param[command] The Discord command to add to the bank.
     * @return[Boolean] Whether the command was successfully added to the registry.
     */
    open fun addCommand(command: ICommandDiscord<*>): Boolean {
        if (enableConfigReadWrite) {
            val configMap = CommandConfig.getConfigs(command, CONFIG_FOLDER)

            // Not all configs were loaded properly, will create missing configs with default values
            if (configMap.containsValue(null)) {
                this.writeConfigs(command, com.github.kvnxiao.kommandant.configurable.CommandConfig.CONFIG_FOLDER)
            }

            this.loadConfigs(command, com.github.kvnxiao.kommandant.configurable.CommandConfig.CONFIG_FOLDER, configMap)
        }
        return cmdBank.addCommand(command)
    }

    /**
     * Load configuration files for a command and all of its subcommands for command properties and permissions
     */
    protected open fun loadConfigs(command: ICommandDiscord<*>, currentPath: String, configMap: Map<String, ByteArray?>) {
        val path = "$currentPath/${command.props.uniqueName}"
        val configByteArr = configMap[path]

        if (configByteArr !== null) {
            // Read the byte array and set new properties for command
            val json: ObjectNode = OBJECT_MAPPER.readTree(configByteArr) as ObjectNode
            val loadedProps: CommandProperties = OBJECT_MAPPER.treeToValue(json[JSON_PROPERTIES_NAME])
            val loadedPerms: PermissionProperties = OBJECT_MAPPER.treeToValue(json[JSON_PERMISSIONS_NAME])
            command.props = loadedProps
            command.perms = loadedPerms
        }

        if (command.hasSubcommands()) {
            command.subCommands.forEach {
                loadConfigs(it as ICommandDiscord<*>, "$currentPath/${command.props.uniqueName}", configMap)
            }
        }
    }

    /**
     * Saves configuration files for a command and all of its subcommands for command properties and permissions
     */
    protected open fun writeConfigs(command: ICommandDiscord<*>, currentPath: String) {
        val path = Paths.get("$currentPath/${command.props.uniqueName}.json")
        val props = OBJECT_MAPPER.valueToTree<JsonNode>(command.props)
        val perms = OBJECT_MAPPER.valueToTree<JsonNode>(command.perms)
        val configJson = OBJECT_MAPPER.createObjectNode()
        configJson.set(JSON_PROPERTIES_NAME, props)
        configJson.set(JSON_PERMISSIONS_NAME, perms)

        // Create file and directories if they do not exist
        try {
            if (Files.notExists(path)) {
                Files.createDirectories(path.parent)
                Files.createFile(path)
                Files.write(path, OBJECT_MAPPER.writeValueAsBytes(configJson))
            }
        } catch (e: IOException) {
            LOGGER.error("An IO error occurred in writing configuration files for $path")
        }

        if (command.hasSubcommands()) {
            command.subCommands.forEach {
                writeConfigs(it as ICommandDiscord<*>, "$currentPath/${command.props.uniqueName}")
            }
        }
    }

}