package com.github.kvnxiao.kommandant.discord.core.command.event

import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.discord.core.command.Permissions
import java.util.*

/**
 * Interface for when a command is called by a user with insufficient permissions.
 */
interface EventMissingPerms {
    /**
     * Method called as a result of a user issuing the command with insufficient permissions.
     */
    fun onMissingPerms(context: CommandContext, missingPerms: EnumSet<Permissions>, event: Any)
}