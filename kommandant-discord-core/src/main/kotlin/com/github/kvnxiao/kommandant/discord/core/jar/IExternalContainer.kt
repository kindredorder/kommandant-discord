package com.github.kvnxiao.kommandant.discord.core.jar

/**
 * A simple, empty interface for specifying a class to be loaded from externally.
 */
interface IExternalContainer