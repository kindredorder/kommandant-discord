package bot

import com.github.kvnxiao.kommandant.command.CommandAnn
import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.discord.core.command.PermissionLevel
import com.github.kvnxiao.kommandant.discord.core.command.Permissions
import com.github.kvnxiao.kommandant.discord.d4j.utility.sendBuffered
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.MessageBuilder

/**
 * Kotlin example of annotation commands showcasing public and private channel responses, and permission level requirements.
 */
open class KotlinPublicPrivateMessageCommands {

    /*
     * This defines a command 'cmd_public' with default prefix '/' and alias 'public'.
     * When called using '/public', the bot will respond with "This is a command where the bot responds in the public channel."
     */
    @CommandAnn(
            uniqueName = "cmd_public",
            aliases = arrayOf("public")
    )
    fun publicCommand(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder) {
        msgBuilder.withContent("This is a command where the bot responds in the public channel.").sendBuffered()
    }

    /*
     * This defines a command 'cmd_public_private' as a subcommand of 'cmd_public', with alias 'cmd_public'.
     * Additionally defining the permission levels using @PermissionLevel, the command forces the bot to reply in
     * a private / direct message to the user, and the command requires a user to have administrator permissions
     * in the channel in order to activate this command.
     *
     * When called using '/public private', the bot will respond in a PRIVATE MESSAGE to the user with "This is a private message."
     */
    @CommandAnn(
            uniqueName = "cmd_public_private",
            aliases = arrayOf("private"),
            parentName = "cmd_public"
    )
    @PermissionLevel(
            forceDmReply = true,
            level = arrayOf(Permissions.ADMINISTRATOR)
    )
    fun privateCommand(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder) {
        msgBuilder.withContent("This is a private message.").sendBuffered()
    }

}