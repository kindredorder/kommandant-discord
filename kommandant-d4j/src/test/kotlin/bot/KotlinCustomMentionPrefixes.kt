package bot

import com.github.kvnxiao.kommandant.command.CommandAnn
import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.discord.d4j.utility.sendBuffered
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.MessageBuilder

/**
 * Created on:   2017-03-17
 * Author:       Kevin Xiao (github.com/kvnxiao)
 *
 */
open class KotlinCustomMentionPrefixes {

    @CommandAnn(
            prefix = "hey %m, ",
            uniqueName = "cmd_custom_prefix",
            aliases = arrayOf("custom")
    )
    fun annotation(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder) {
        msgBuilder.withContent("This is a command activated using a custom mention prefix.").sendBuffered()
    }

}