package bot

import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.command.CommandProperties
import com.github.kvnxiao.kommandant.discord.core.command.PermissionProperties
import com.github.kvnxiao.kommandant.discord.d4j.CommandBuilderD4J
import com.github.kvnxiao.kommandant.discord.d4j.CommandBuilderD4J.Companion.execute
import com.github.kvnxiao.kommandant.discord.d4j.CommandD4J
import com.github.kvnxiao.kommandant.discord.d4j.KommandantD4J
import com.github.kvnxiao.kommandant.discord.d4j.utility.sendBuffered
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import sx.blah.discord.api.ClientBuilder
import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.api.events.EventSubscriber
import sx.blah.discord.handle.impl.events.ReadyEvent
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.MessageBuilder
import java.nio.file.Files
import java.nio.file.Paths

/**
 * Kotlin-based Discord bot test.
 */
open class BotTest {

    private val client: IDiscordClient
    private val LOGGER: Logger = LoggerFactory.getLogger(BotTest::class.java)
    private val kommandant: KommandantD4J = KommandantD4J()

    init {
        val token = String(Files.readAllBytes(Paths.get("token.txt")))
        println("Token read as: $token")
        client = ClientBuilder().withToken(token).build()
        client.dispatcher.registerListener(this)
        client.dispatcher.registerListener(kommandant)
        registerCommands()
    }

    fun registerCommands() {
        /*
         * Commands by default use the prefix '/' unless otherwise explicitly stated when you create your command.
         */

        /*
         * Constructor based commands.
         *
         * This command uses a CommandD4J constructor to build commands the "old-fashioned" way.
         * Very verbose and highly NOT recommended. It is more preferable to use the CommandBuilderD4J builder class
         * as shown directly below.
         */
        kommandant.addCommand(object : CommandD4J<Unit>(CommandProperties(uniqueName = "cmd_constructor", aliases = listOf("constructor")), PermissionProperties()) {
            override fun executeWith(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder) {
                /*
                 * This defines a command 'cmd_constructor' with prefix '/' and alias 'constructor',
                 * and default permissions (default = a user needs READ message and WRITE message permissions)
                 *
                 * The command can be called by a user message: '/constructor',
                 * and the bot will reply with "This is a command created using the command constructor."
                 */
                msgBuilder.withContent("This is a command created using the command constructor.").sendBuffered()
            }
        })

        /*
         * Commands created using the CommandBuilderD4J builder.
         *
         * The builder assigns default values to a new Command if those properties are unspecified.
         * It only requires a minimum of the "uniqueName" identifier string for the command, and the execution method.
         * If no alias is provided, the builder will use the command's unique name as the alias.
         * Everything else that is unspecified will default to values defined in CommandDefaults and PermissionDefaults.
         *
         * In Kotlin, use the CommandBuilderD4J.Companion.execute { } lambda function to easily create the executable method.
         */
        kommandant.addCommand(CommandBuilderD4J<Any>("cmd_builder").withPrefix("!").withAliases("builder")
                .build(execute { context, event, msgBuilder ->
                    /**
                     *  This defines a command 'cmd_builder' with prefix '!' and alias 'builder', and default permissions.
                     * It can be called by a user message: '!builder',
                     * and the bot will reply with "This is a command created using a builder."
                     */
                    msgBuilder.withContent("This is a command created using a builder.").sendBuffered()
                }))

        /*
         * Register annotation based commands like so. This parses commands defined in other classes.
         * Check out these classes used below to see how to define them.
         */
        kommandant.addAnnotatedCommands(KotlinAnnotatedCommands::class)
        kommandant.addAnnotatedCommands(KotlinPublicPrivateMessageCommands::class)
        kommandant.addAnnotatedCommands(KotlinCustomMentionPrefixes::class)

        /*
         * Adding sub-commands is as simple as calling CommandD4J#addSubcommand(subCommand), and then adding the main command to Kommandant.
         */
        kommandant.addCommand(CommandBuilderD4J<Unit>("cmd_main").withAliases("main").build(execute { context, event, msgBuilder ->
            /*
             * This defines a command 'cmd_main' that can be called by a user message: '/main'
             * The bot will reply with "This is a main command."
             */
            msgBuilder.withContent("This is a main command.").sendBuffered()

        }).addSubcommand(CommandBuilderD4J<Unit>("cmd_main_sub").withAliases("sub").build(execute { context, event, msgBuilder ->
            /*
             * This defines a command 'cmd_main_sub' as a subcommand of 'cmd_main', which can be called by: '/main sub'
             * The bot will reply with "This is a subcommand of a main command."
             */
            msgBuilder.withContent("This is a subcommand of a main command.").sendBuffered()
        })))

        /*
         * Load external commands. This is called by the Kommandant constructor by default.
         *
         * This issues a load operation to load commands externally defined in a .jar file.
         * Kommandant will search for .jar files located relative to where your root project directory is,
         * under the subfolder defined in 'konfig.json' for the JSON key-value "cmdJarFolder",
         * which normally defaults to "kommandant/".
         */
        kommandant.loadExternalCommands()
    }

    fun login() = client.login()

    @EventSubscriber
    fun onReady(event: ReadyEvent) {
        LOGGER.info("The bot is ONLINE!")
    }

    @EventSubscriber
    fun onMessage(event: MessageReceivedEvent) {
        LOGGER.info(event.message.content)
    }

}

fun main(args: Array<String>) {
    val bot: BotTest = BotTest()
    bot.login()
}