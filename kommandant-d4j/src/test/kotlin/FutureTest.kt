import com.github.kvnxiao.kommandant.command.CommandBuilder
import com.github.kvnxiao.kommandant.command.CommandBuilder.Companion.execute
import com.github.kvnxiao.kommandant.discord.d4j.KommandantD4J
import org.junit.Test

/**
 * Created on:   2017-03-10
 * Author:       Kevin Xiao (github.com/kvnxiao)
 *
 */
open class FutureTest {

    private val k = KommandantD4J()

    @Test
    fun testAsyncFuture() {
        k.addCommand(CommandBuilder<Unit>("test").withAliases("test").build(execute { context, opt ->
            Thread.sleep(300)
            println("Hello world")
        }))
        repeat(16) {
            k.processAsync<Unit>("/test")
        }

        Thread.sleep(1200)
    }

}