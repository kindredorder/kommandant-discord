package bot;

import com.github.kvnxiao.kommandant.command.CommandAnn;
import com.github.kvnxiao.kommandant.command.CommandContext;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;

import static com.github.kvnxiao.kommandant.discord.d4j.utility.SendUtilityKt.sendBuffered;

/**
 * Java example of annotation-based commands.
 */
public class JavaAnnotatedCommands {

    /*
     * An annotation defined command must use the annotation @CommandAnn(...) to specify the properties of the command.
     *
     * Additionally, it is optional to include the annotation @PermissionLevel(...) to specify the permission properties
     * of the Discord command. If unspecified, the commands will use the default permission values defined in PermissionDefaults class
     *
     * All prefixes default to '/' if not defined explicitly in the @CommandAnn.
     */

    /*
     * This defines a command named 'annotation' with default prefix '/' and the alias 'annotation'.
     * To activate it, a user sends a message '/annotation' on Discord and the bot will reply with 'This is an annotation based command.'
     */
    @CommandAnn(
            uniqueName = "cmd_annotation",
            aliases = "annotation"
    )
    public void annotation(CommandContext context, MessageReceivedEvent event, MessageBuilder msgBuilder) {
        sendBuffered(msgBuilder.withContent("This is an annotation based command."));
    }

    /*
     * This defines a subcommand of the above 'cmd_annotation'. Notice how the parentName is specified in this
     * command's annotation; this is to properly link this command as a subcommand of 'cmd_annotation'.
     *
     * To activate this subcommand, a user sends a message '/annotation sub' and the bot will reply with 'This is the
     * subcommand of the annotation based command.'
     */
    @CommandAnn(
            uniqueName = "cmd_annotation_sub",
            aliases = "sub",
            parentName = "cmd_annotation"
    )
    public void annotationSubcommand(CommandContext context, MessageReceivedEvent event, MessageBuilder msgBuilder) {
        sendBuffered(msgBuilder.withContent("This is the subcommand of the annotation based command."));
    }

}