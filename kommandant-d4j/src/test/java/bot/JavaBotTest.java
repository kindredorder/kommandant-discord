package bot;

import com.github.kvnxiao.kommandant.command.CommandContext;
import com.github.kvnxiao.kommandant.command.CommandDefaults;
import com.github.kvnxiao.kommandant.command.CommandProperties;
import com.github.kvnxiao.kommandant.discord.core.command.PermissionDefaults;
import com.github.kvnxiao.kommandant.discord.core.command.PermissionProperties;
import com.github.kvnxiao.kommandant.discord.d4j.CommandBuilderD4J;
import com.github.kvnxiao.kommandant.discord.d4j.CommandD4J;
import com.github.kvnxiao.kommandant.discord.d4j.KommandantD4J;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventSubscriber;
import sx.blah.discord.handle.impl.events.ReadyEvent;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent;
import sx.blah.discord.util.MessageBuilder;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;

import static com.github.kvnxiao.kommandant.discord.d4j.utility.SendUtilityKt.sendBuffered;

/**
 * Java-based Discord bot test.
 */
public class JavaBotTest {

    private IDiscordClient client;
    private Logger LOGGER = LoggerFactory.getLogger(JavaBotTest.class);
    private KommandantD4J kommandant = new KommandantD4J();

    public JavaBotTest() {
        try {
            String token = new String(Files.readAllBytes(Paths.get("token.txt")));
            System.out.println("Token read as: " + token);
            client = new ClientBuilder().withToken(token).build();
        } catch (IOException ignored) {}
        client.getDispatcher().registerListener(this);
        client.getDispatcher().registerListener(kommandant);
        this.registerCommands();
    }

    public static void main(String[] args) {
        JavaBotTest bot = new JavaBotTest();
        bot.login();
    }

    public void registerCommands() {
        /*
         * Commands by default use the prefix '/' unless otherwise explicitly stated when you create your command.
         */

        /*
         * Constructor based commands.
         *
         * This command uses a CommandD4J constructor to build commands the "old-fashioned" way.
         * Very verbose and highly NOT recommended. It is more preferable to use the CommandBuilderD4J builder class
         * as shown directly below.
         */
        kommandant.addCommand(new CommandD4J<Void>(
                new CommandProperties(CommandDefaults.PREFIX, "cmd_constructor", CommandDefaults.NO_DESCRIPTION, CommandDefaults.NO_USAGE, CommandDefaults.EXEC_WITH_SUBCOMMANDS, CommandDefaults.IS_DISABLED, Collections.singletonList("constructor")),
                new PermissionProperties(PermissionDefaults.REQUIRE_MENTION, PermissionDefaults.ALLOW_DM, PermissionDefaults.FORCE_DM_REPLY, PermissionDefaults.REMOVE_CALL_MSG, PermissionDefaults.DEFAULT_PERMS_RW, PermissionDefaults.RATE_LIMIT_PERIOD_MS, PermissionDefaults.TOKENS_PER_PERIOD, PermissionDefaults.RATE_LIMIT_ON_GUILD)) {
            @Override
            public Void executeWith(@NotNull CommandContext context, @NotNull MessageReceivedEvent event, @NotNull MessageBuilder msgBuilder) throws InvocationTargetException, IllegalAccessException {
                /* This defines a command 'cmd_constructor' with prefix '/' and alias 'constructor',
                 * and default permissions (default = a user needs READ message and WRITE message permissions)
                 *
                 * The command can be called by a user message: '/constructor',
                 * and the bot will reply with "This is a command created using the command constructor."
                 */
                sendBuffered(msgBuilder.withContent("This is a command created using the command constructor."));
                return null;
            }
        });

        /*
         * Commands created using the CommandBuilderD4J builder.
         *
         * The builder assigns default values to a new Command if those properties are unspecified.
         * It only requires a minimum of the "uniqueName" identifier string for the command, and the execution method.
         * If no alias is provided, the builder will use the command's unique name as the alias.
         * Everything else that is unspecified will default to values defined in CommandDefaults and PermissionDefaults.
         */
        kommandant.addCommand(new CommandBuilderD4J<Void>("cmd_builder").withPrefix("!").withAliases("builder")
                .build((context, event, msgBuilder) -> {
                    /*
                     * This defines a command 'cmd_builder' with prefix '!' and alias 'builder', and default permissions.
                     * It can be called by a user message: '!builder',
                     * and the bot will reply with "This is a command created using a builder."
                     */
                    sendBuffered(msgBuilder.withContent("This is a command created using a builder."));
                    return null;
                }));

        /*
         * Register annotation based commands like so. This parses commands defined in other classes.
         * Check out these classes used below to see how to define them.
         */
        kommandant.addAnnotatedCommands(JavaAnnotatedCommands.class);
        kommandant.addAnnotatedCommands(JavaPublicPrivateMessageCommands.class);

        /*
         * Adding sub-commands is as simple as calling CommandD4J#addSubcommand(subCommand), and then adding the main command to Kommandant.
         */
        kommandant.addCommand(new CommandBuilderD4J<Void>("cmd_main").withAliases("main").build((context, event, msgBuilder) -> {
            /*
             * This defines a command 'cmd_main' that can be called by a user message: '/main'
             * The bot will reply with "This is a main command."
             */
            sendBuffered(msgBuilder.withContent("This is a main command."));
            return null;
        }).addSubcommand(new CommandBuilderD4J<Void>("cmd_main_sub").withAliases("sub").build((context, event, msgBuilder) -> {
            /*
             * This defines a command 'cmd_main_sub' as a subcommand of 'cmd_main', which can be called by: '/main sub'
             * The bot will reply with "This is a subcommand of a main command."
             */
            sendBuffered(msgBuilder.withContent("This is a subcommand of a main command."));
            return null;
        })));

        /*
         * Load external commands. This is called by the Kommandant constructor by default.
         *
         * This issues a load operation to load commands externally defined in a .jar file.
         * Kommandant will search for .jar files located relative to where your root project directory is,
         * under the subfolder defined in 'konfig.json' for the JSON key-value "cmdJarFolder",
         * which normally defaults to "kommandant/".
         */
        kommandant.loadExternalCommands();
    }

    public void login() {
        this.client.login();
    }

    @EventSubscriber
    public void onReady(ReadyEvent event) {
        LOGGER.info("The bot is ONLINE!");
    }

    @EventSubscriber
    public void onMessage(MessageReceivedEvent event) {
        LOGGER.info(event.getMessage().getContent());
    }

}
