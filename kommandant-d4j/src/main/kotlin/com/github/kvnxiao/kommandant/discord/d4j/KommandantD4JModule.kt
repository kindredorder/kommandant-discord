package com.github.kvnxiao.kommandant.discord.d4j

import sx.blah.discord.api.IDiscordClient
import sx.blah.discord.modules.IModule

/**
 * Discord Module
 */
class KommandantD4JModule(private val kommandant: KommandantD4J = KommandantD4J()) : IModule {

    private var client: IDiscordClient? = null

    companion object {
        const val NAME = "KommandantDiscord for Discord4J"
        const val AUTHOR = "kvnxiao"
        const val VERSION = "DEVELOPMENT"
        const val MIN_D4J_VERSION = "2.7.0"
    }

    override fun enable(client: IDiscordClient?): Boolean {
        this.client = client
        this.client?.dispatcher?.registerListener(this.kommandant)
        this.kommandant.loadExternalCommands()
        return true
    }

    override fun getName(): String = NAME

    override fun getVersion(): String = KommandantD4JModule::class.java.`package`.implementationVersion ?: VERSION

    override fun getMinimumDiscord4JVersion(): String = MIN_D4J_VERSION

    override fun getAuthor(): String = AUTHOR

    override fun disable() {
        this.client?.dispatcher?.unregisterListener(this.kommandant)
    }

}