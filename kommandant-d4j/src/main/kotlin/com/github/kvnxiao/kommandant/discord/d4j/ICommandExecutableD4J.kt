package com.github.kvnxiao.kommandant.discord.d4j

import com.github.kvnxiao.kommandant.command.CommandContext
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.MessageBuilder
import java.lang.reflect.InvocationTargetException

/**
 * An interface that defines the methods a command executor needs when used with Discord4J.
 */
@FunctionalInterface
interface ICommandExecutableD4J<out T> {

    /**
     * Executes the Discord4J command with the provided command context, MessageReceivedEvent, and MessageBuilder.
     *
     * @param[context] The context of the command, containing the calling alias and any args it may have.
     * @param[event] The message received event.
     * @param[msgBuilder] The message builder for replying with.
     * @throws[InvocationTargetException] When the method invocation failed.
     * @throws[IllegalAccessException] When the method could not be accessed through reflection.
     * @return[T] Returns the result of the execution.
     */
    @Throws(InvocationTargetException::class, IllegalAccessException::class)
    fun executeWith(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder): T

}