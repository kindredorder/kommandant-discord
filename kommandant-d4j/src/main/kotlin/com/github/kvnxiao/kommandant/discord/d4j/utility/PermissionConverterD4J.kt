package com.github.kvnxiao.kommandant.discord.d4j.utility

import com.github.kvnxiao.kommandant.discord.core.command.Permissions
import com.github.kvnxiao.kommandant.discord.d4j.utility.PermissionConverterD4J.permissionMapFromD4J
import com.github.kvnxiao.kommandant.discord.d4j.utility.PermissionConverterD4J.permissionMapToD4J
import java.util.*

/**
 * Type alias to represent Discord4J Permissions type.
 */
typealias PermissionsD4J = sx.blah.discord.handle.obj.Permissions

/**
 * Adapter to convert from [Permissions] enums to Discord4J Permissions enums.
 */
object PermissionConverterD4J {
    @JvmField
    val permissionMapToD4J: Map<Permissions, PermissionsD4J> = mapOf(
            Permissions.CREATE_INVITE to PermissionsD4J.CREATE_INVITE,
            Permissions.KICK_MEMBERS to PermissionsD4J.KICK,
            Permissions.BAN_MEMBERS to PermissionsD4J.BAN,
            Permissions.ADMINISTRATOR to PermissionsD4J.ADMINISTRATOR,
            Permissions.MANAGE_CHANNELS to PermissionsD4J.MANAGE_CHANNELS,
            Permissions.MANAGE_CHANNEL_SPECIFIC to PermissionsD4J.MANAGE_CHANNEL,
            Permissions.MANAGE_SERVER to PermissionsD4J.MANAGE_SERVER,
            Permissions.MESSAGE_ADD_REACTIONS to PermissionsD4J.ADD_REACTIONS,
            Permissions.MESSAGE_READ to PermissionsD4J.READ_MESSAGES,
            Permissions.MESSAGE_WRITE to PermissionsD4J.SEND_MESSAGES,
            Permissions.MESSAGE_TTS to PermissionsD4J.SEND_TTS_MESSAGES,
            Permissions.MANAGE_MESSAGES to PermissionsD4J.MANAGE_MESSAGES,
            Permissions.MESSAGE_EMBED_LINKS to PermissionsD4J.EMBED_LINKS,
            Permissions.MESSAGE_ATTACH_FILES to PermissionsD4J.ATTACH_FILES,
            Permissions.MESSAGE_READ_HISTORY to PermissionsD4J.READ_MESSAGE_HISTORY,
            Permissions.MESSAGE_MENTION_EVERYONE to PermissionsD4J.MENTION_EVERYONE,
            Permissions.MESSAGE_EXTERNAL_EMOJIS to PermissionsD4J.USE_EXTERNAL_EMOJIS,
            Permissions.VOICE_CONNECT to PermissionsD4J.VOICE_CONNECT,
            Permissions.VOICE_SPEAK to PermissionsD4J.VOICE_SPEAK,
            Permissions.VOICE_MUTE_MEMBERS to PermissionsD4J.VOICE_MUTE_MEMBERS,
            Permissions.VOICE_DEAFEN_MEMBERS to PermissionsD4J.VOICE_DEAFEN_MEMBERS,
            Permissions.VOICE_MOVE_MEMBERS to PermissionsD4J.VOICE_MOVE_MEMBERS,
            Permissions.VOICE_USE_VAD to PermissionsD4J.VOICE_USE_VAD,
            Permissions.CHANGE_NICKNAME to PermissionsD4J.CHANGE_NICKNAME,
            Permissions.MANAGE_NICKNAMES to PermissionsD4J.MANAGE_NICKNAMES,
            Permissions.MANAGE_ROLES to PermissionsD4J.MANAGE_ROLES,
            Permissions.MANAGE_PERMISSIONS to PermissionsD4J.MANAGE_PERMISSIONS,
            Permissions.MANAGE_WEBHOOKS to PermissionsD4J.MANAGE_WEBHOOKS,
            Permissions.MANAGE_EMOJIS to PermissionsD4J.MANAGE_EMOJIS)

    @JvmField
    val permissionMapFromD4J: Map<PermissionsD4J, Permissions> = mapOf(
            PermissionsD4J.CREATE_INVITE to Permissions.CREATE_INVITE,
            PermissionsD4J.KICK to Permissions.KICK_MEMBERS,
            PermissionsD4J.BAN to Permissions.BAN_MEMBERS,
            PermissionsD4J.ADMINISTRATOR to Permissions.ADMINISTRATOR,
            PermissionsD4J.MANAGE_CHANNELS to Permissions.MANAGE_CHANNELS,
            PermissionsD4J.MANAGE_CHANNEL to Permissions.MANAGE_CHANNEL_SPECIFIC,
            PermissionsD4J.MANAGE_SERVER to Permissions.MANAGE_SERVER,
            PermissionsD4J.ADD_REACTIONS to Permissions.MESSAGE_ADD_REACTIONS,
            PermissionsD4J.READ_MESSAGES to Permissions.MESSAGE_READ,
            PermissionsD4J.SEND_MESSAGES to Permissions.MESSAGE_WRITE,
            PermissionsD4J.SEND_TTS_MESSAGES to Permissions.MESSAGE_TTS,
            PermissionsD4J.MANAGE_MESSAGES to Permissions.MANAGE_MESSAGES,
            PermissionsD4J.EMBED_LINKS to Permissions.MESSAGE_EMBED_LINKS,
            PermissionsD4J.ATTACH_FILES to Permissions.MESSAGE_ATTACH_FILES,
            PermissionsD4J.READ_MESSAGE_HISTORY to Permissions.MESSAGE_READ_HISTORY,
            PermissionsD4J.MENTION_EVERYONE to Permissions.MESSAGE_MENTION_EVERYONE,
            PermissionsD4J.USE_EXTERNAL_EMOJIS to Permissions.MESSAGE_EXTERNAL_EMOJIS,
            PermissionsD4J.VOICE_CONNECT to Permissions.VOICE_CONNECT,
            PermissionsD4J.VOICE_SPEAK to Permissions.VOICE_SPEAK,
            PermissionsD4J.VOICE_MUTE_MEMBERS to Permissions.VOICE_MUTE_MEMBERS,
            PermissionsD4J.VOICE_DEAFEN_MEMBERS to Permissions.VOICE_DEAFEN_MEMBERS,
            PermissionsD4J.VOICE_MOVE_MEMBERS to Permissions.VOICE_MOVE_MEMBERS,
            PermissionsD4J.VOICE_USE_VAD to Permissions.VOICE_USE_VAD,
            PermissionsD4J.CHANGE_NICKNAME to Permissions.CHANGE_NICKNAME,
            PermissionsD4J.MANAGE_NICKNAMES to Permissions.MANAGE_NICKNAMES,
            PermissionsD4J.MANAGE_ROLES to Permissions.MANAGE_ROLES,
            PermissionsD4J.MANAGE_PERMISSIONS to Permissions.MANAGE_PERMISSIONS,
            PermissionsD4J.MANAGE_WEBHOOKS to Permissions.MANAGE_WEBHOOKS,
            PermissionsD4J.MANAGE_EMOJIS to Permissions.MANAGE_EMOJIS)
}

fun EnumSet<Permissions>.toD4JPermissions(): EnumSet<PermissionsD4J> {
    val d4jPerms: EnumSet<PermissionsD4J> = EnumSet.noneOf(PermissionsD4J::class.java)
    val list: List<PermissionsD4J> = this.map<Permissions, PermissionsD4J> { permissionMapToD4J[it]!! }
    d4jPerms.addAll(list)
    return d4jPerms
}

fun EnumSet<PermissionsD4J>.toPermissions(): EnumSet<Permissions> {
    val perms: EnumSet<Permissions> = EnumSet.noneOf(Permissions::class.java)
    val list: List<Permissions> = this.map<PermissionsD4J, Permissions> { permissionMapFromD4J[it]!! }
    perms.addAll(list)
    return perms
}
