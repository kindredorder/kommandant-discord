package com.github.kvnxiao.kommandant.discord.d4j

import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.command.CommandProperties
import com.github.kvnxiao.kommandant.discord.core.command.ICommandDiscord
import com.github.kvnxiao.kommandant.discord.core.command.PermissionProperties
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.handle.obj.IChannel
import sx.blah.discord.util.MessageBuilder

/**
 * An abstract class extending [ICommandDiscord] representing a Discord4J command with properties and permissions.
 *
 * @property[props] The command properties describing this command.
 * @property[perms] The permission properties describing how this command should be called.
 * @see[ICommandExecutableD4J]
 */
abstract class CommandD4J<T>(props: CommandProperties, perms: PermissionProperties) : ICommandDiscord<T>(props, perms), ICommandExecutableD4J<T> {

    /**
     * Overrides the default execution operation to execute specifically for Discord4J implementations with
     * MessageReceivedEvent and MessageBuilder visible to the command.
     */
    final override fun execute(context: CommandContext, vararg opt: Any?): T {
        val event = opt[0] as MessageReceivedEvent
        val client = event.client
        val channel: IChannel = if (this.perms.forceDmReply) client.getOrCreatePMChannel(event.message.author) else event.message.channel
        return this.executeWith(context, event, MessageBuilder(client).withChannel(channel))
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is CommandD4J<*>) return false

        if (props != other.props) return false
        if (subCommands != other.subCommands) return false
        if (perms != other.perms) return false

        return true
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }
}