package com.github.kvnxiao.kommandant.discord.d4j

import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.discord.core.KommandantDiscord
import com.github.kvnxiao.kommandant.discord.core.jar.ExternalJarLoader
import com.github.kvnxiao.kommandant.discord.core.jar.IExternalCommandLoader
import com.github.kvnxiao.kommandant.discord.core.jar.IExternalContainer
import com.github.kvnxiao.kommandant.impl.CommandBank
import com.github.kvnxiao.kommandant.utility.SplitString
import sx.blah.discord.api.events.IListener
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.handle.obj.IMessage
import java.lang.reflect.InvocationTargetException

/**
 * The main Kommandant aggregation class for use with the Discord4J library.
 *
 * This class implements a Discord4J IListener<MessageReceivedEvent> and should be registered by the bot client accordingly.
 */
open class KommandantD4J : KommandantDiscord(enableConfigReadWrite = true, cmdParser = CommandParserD4J(), cmdExecutor = CommandExecutorD4J()), IExternalCommandLoader, IListener<MessageReceivedEvent> {

    // Load external commands if this setting is true
    init {
        if (this.config.loadExternalCommands) this.loadExternalCommands()
    }

    /**
     * MessageReceivedEvent handler which parses the message contents for a command match and executes it in another thread when a match is found.
     *
     * @param[event] The message received event.
     */
    override fun handle(event: MessageReceivedEvent) {
        threadPool.submit {
            val message = event.message
            val messageContent = message.content

            // First split to check for bot mention
            val (firstStr, secondStr) = SplitString(messageContent)
            if (firstStr !== null) {
                // Check for bot mention
                val hasBotMention = hasBotMention(firstStr, message)
                val content = if (hasBotMention) secondStr else messageContent
                if (content !== null) processPrimaryPrefix(content, event, hasBotMention)
            }
        }
    }

    /**
     * Primary message processor which attempts to discover a command with a normal prefix from the user's message.
     * Will send to secondary message processor to discover a command with a custom mention prefix.
     *
     * @see[processCustomPrefix]
     */
    open fun processPrimaryPrefix(content: String, event: MessageReceivedEvent, hasBotMention: Boolean) {
        val (alias, args) = SplitString(content)
        if (alias !== null) {
            val command: CommandD4J<*>? = this.getCommand(alias) as CommandD4J<*>?
            if (command !== null) {
                sendToExecutor(command, alias, args, event, hasBotMention)
            } else {
                processCustomPrefix(content, event, hasBotMention)
            }
        }
    }

    /**
     * Secondary message processor which attempts to discover a command with a custom mention prefix.
     *
     * @param[content] The message content string.
     * @param[event] The message received event.
     * @param[hasBotMention] Whether the message originally included a mention of the bot at the beginning of the string.
     * @see[processPrimaryPrefix]
     */
    open fun processCustomPrefix(content: String, event: MessageReceivedEvent, hasBotMention: Boolean) {
        // Return if we have no custom mentions set and we are not including bot mentions as a custom mention
        if (!discordSettings.includeMentionPrefix && discordSettings.isMentionsEmpty()) return

        // Otherwise, continue
        val botMention = event.client.ourUser.mention(false)
        val botMentionNickname = event.client.ourUser.mention(true)

        val replace = content.replace(
                if (discordSettings.includeMentionPrefix) {
                    if (discordSettings.isMentionsEmpty())
                        "($botMention|$botMentionNickname)".toRegex()
                    else
                        "($botMention|$botMentionNickname|${discordSettings.getMentionsForRegex()})".toRegex()
                } else {
                    "(${discordSettings.getMentionsForRegex()})".toRegex()
                }, CUSTOM_MENTION_KEY)
        this.getPrefixes().forEach {
            if (replace.startsWith(it)) {
                val nextContent = replace.substring(it.length)
                val (alias, args) = SplitString(nextContent)

                if (alias != null) {
                    val command: CommandD4J<*>? = this.getCommand(it + alias) as CommandD4J<*>?
                    if (command !== null) {
                        sendToExecutor(command, alias, args, event, hasBotMention)
                    }
                }
            }
        }
    }

    /**
     * Collects the command from the message processing methods and builds a command context to send to the command executor.
     * It will additionally log whether the message was sent through a direct message or public channel.
     *
     * @param[command] The command to execute, received from the message processor.
     * @param[alias] The alias used to call the command.
     * @param[args] The arguments provided for the command.
     * @param[event] The message received event.
     * @param[hasBotMention] Whether the message originally included a mention of the bot at the beginning of the string.
     */
    open fun sendToExecutor(command: CommandD4J<*>, alias: String, args: String?, event: MessageReceivedEvent, hasBotMention: Boolean) {
        val username = event.message.author.name
        val isDm = event.message.channel.isPrivate

        // Log command execution for private (Direct Message) and non-private channels
        if (isDm) LOGGER.debug("'$username' attempted to execute command '$command'") else LOGGER.debug("'$username' attempted to execute command '$command' in guild '${event.message.guild.name}', channel '${event.channel}'")

        this.processCommand<Any?>(command, CommandContext(alias, args, command), event,
                // Send over permission check info
                isDm, hasBotMention)
    }

    /**
     * Checks if the message received contains a bot mention at the beginning.
     */
    protected open fun hasBotMention(content: String, message: IMessage): Boolean {
        val botMention = message.client.ourUser.mention(false)
        val botMentionNickname = message.client.ourUser.mention(true)
        return content == botMention || content == botMentionNickname
    }

    /**
     * Loads external Discord4J commands from the classpath on runtime.
     */
    override fun loadExternalCommands() {
        ExternalJarLoader.loadJars(this.config.cmdJarFolder).forEach { k, v ->
            LOGGER.debug("Loading $k for commands...")
            v.forEach {
                try {
                    val classInstance: Class<*> = Class.forName(it)
                    if (CommandD4J::class.java.isAssignableFrom(classInstance) && classInstance != ICommandExecutableD4J::class.java) {
                        // Load class itself as a command
                        val command = createCommandFromClass(classInstance)
                        if (command !== null) this.addCommand(command)
                    } else if (IExternalContainer::class.java.isAssignableFrom(classInstance) && classInstance != IExternalContainer::class.java) {
                        // Parse annotations for commands
                        this.addAnnotatedCommands(classInstance)
                    }
                } catch (ignored: ClassNotFoundException) {
                    // Ignore errors for class not found.
                }
            }
        }
    }

    /**
     * Creates a Discord4J command from a java Class object
     *
     * @param[classInstance] The CommandD4J class instance.
     * @return[CommandD4J] The Discord4J command produced from this class.
     */
    open protected fun createCommandFromClass(classInstance: Class<*>): CommandD4J<*>? {
        val constructors = classInstance.constructors
        constructors
                .map { it.parameterTypes }
                .forEach {
                    if (it.size == 1 && it[0].isAssignableFrom(CommandBank::class.java)) {
                        try {
                            val bankConstructor = classInstance.getConstructor(CommandBank::class.java)
                            return bankConstructor.newInstance(this.cmdBank) as CommandD4J<*>
                        } catch (e: InstantiationException) {
                            LOGGER.error("External command class ${classInstance.simpleName} could not be instantiated with a CommandBank reliant constructor.")
                        } catch (e: IllegalAccessException) {
                            LOGGER.error("External command class ${classInstance.simpleName} could not be instantiated with a CommandBank reliant constructor.")
                        } catch (e: NoSuchMethodException) {
                            LOGGER.debug("External command class ${classInstance.simpleName} could not be instantiated with a CommandBank reliant constructor.")
                        } catch (e: InvocationTargetException) {
                            LOGGER.debug("External command class ${classInstance.simpleName} could not be instantiated with a CommandBank reliant constructor.")
                        }
                    } else {
                        // Create command with empty constructor
                        try {
                            return classInstance.newInstance() as CommandD4J<*>
                        } catch (e: InstantiationException) {
                            LOGGER.error("External command class ${classInstance.simpleName} could not be instantiated with a no-args constructor!")
                        } catch (e: IllegalAccessException) {
                            LOGGER.error("External command class ${classInstance.simpleName} could not be instantiated with a no-args constructor!")
                        }
                    }
                }
        return null
    }

}