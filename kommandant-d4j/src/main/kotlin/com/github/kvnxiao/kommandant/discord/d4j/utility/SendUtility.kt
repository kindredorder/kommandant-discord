package com.github.kvnxiao.kommandant.discord.d4j.utility

import sx.blah.discord.util.MessageBuilder
import sx.blah.discord.util.RequestBuffer

/**
 * Created on:   2017-03-11
 * Author:       Kevin Xiao (github.com/kvnxiao)
 *
 */

/**
 * Utility method to wrap a MessageBuilder's send() or build() function with a request buffer to avoid rate limit issues.
 */
fun MessageBuilder.sendBuffered() {
    RequestBuffer.request {
        this.build()
    }
}