package com.github.kvnxiao.kommandant.discord.d4j

import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.command.CommandDefaults
import com.github.kvnxiao.kommandant.command.CommandProperties
import com.github.kvnxiao.kommandant.discord.core.command.PermissionDefaults
import com.github.kvnxiao.kommandant.discord.core.command.PermissionProperties
import com.github.kvnxiao.kommandant.discord.core.command.Permissions
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.MessageBuilder
import java.lang.reflect.InvocationTargetException
import java.util.*

/**
 * A builder class that helps define a Discord4J command without needing to specify all the properties for the [CommandD4J] constructor.
 *
 * @constructor Constructor which defines the [uniqueName] of the command to build.
 * @property[uniqueName] The unique name of the command to build.
 */
class CommandBuilderD4J<T>(private val uniqueName: String) {

    /**
     * Constructor which defines the [prefix] and [uniqueName] of the command to build.
     */
    constructor(prefix: String, uniqueName: String) : this(uniqueName) {
        this.prefix = prefix
    }

    // Command properties
    /**
     * The aliases of the command to build, represented as a (nullable) list of strings. If the list is null, the builder
     * will default to using the command's [uniqueName] as the command's alias.
     */
    private var aliases: List<String>? = null

    /**
     * The description of the command to build. Defaults to [CommandDefaults.NO_DESCRIPTION].
     */
    private var description: String = CommandDefaults.NO_DESCRIPTION

    /**
     * The usage of the command to build. Defaults to [CommandDefaults.NO_USAGE].
     */
    private var usage: String = CommandDefaults.NO_USAGE

    /**
     * The prefix of the command to build. Defaults to [CommandDefaults.PREFIX].
     */
    private var prefix: String = CommandDefaults.PREFIX

    /**
     * Whether the command should execute alongside its subcommand or be skipped when subcommands are processed.
     * Defaults to [CommandDefaults.EXEC_WITH_SUBCOMMANDS].
     */
    private var execWithSubcommands: Boolean = CommandDefaults.EXEC_WITH_SUBCOMMANDS

    /**
     * Whether the command is disabled. Defaults to [CommandDefaults.IS_DISABLED].
     */
    private var isDisabled: Boolean = CommandDefaults.IS_DISABLED

    // Permission properties
    /**
     * The permission level required to call this command. Defaults to [PermissionDefaults.DEFAULT_PERMS_RW].
     */
    private var permissionLevels: EnumSet<Permissions> = PermissionDefaults.DEFAULT_PERMS_RW
    /**
     * Whether the command requires a bot mention in order to activate. Defaults to [PermissionDefaults.REQUIRE_MENTION]
     */
    private var reqMention: Boolean = PermissionDefaults.REQUIRE_MENTION

    /**
     * Whether the command is allowed to be activated through a direct message to the bot. Defaults to [PermissionDefaults.ALLOW_DM]
     */
    private var allowDm: Boolean = PermissionDefaults.ALLOW_DM

    /**
     * Whether the command is forced to reply in a private chat or not. Defaults to [PermissionDefaults.FORCE_DM_REPLY]
     */
    private var forceDmReply: Boolean = PermissionDefaults.FORCE_DM_REPLY

    /**
     * Whether the calling message is removed after activating the command. Defaults to [PermissionDefaults.REMOVE_CALL_MSG]
     */
    private var removeCallMsg: Boolean = PermissionDefaults.REMOVE_CALL_MSG

    /**
     * The time period for rate limiting a command in milliseconds. Defaults to 1000 ms or 1 second.
     */
    private var rateLimitMs: Long = PermissionDefaults.RATE_LIMIT_PERIOD_MS

    /**
     * The number of valid tokens or calls per rate limit period for a command. Defaults to 3 calls per second.
     */
    private var tokensPerPeriod: Long = PermissionDefaults.TOKENS_PER_PERIOD

    /**
     * Whether the command is rate limited on a guild-global scale or on a per user scale.
     */
    private var ratelimitOnGuild: Boolean = PermissionDefaults.RATE_LIMIT_ON_GUILD

    /**
     * Sets the prefix of the command to build.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun withPrefix(prefix: String): CommandBuilderD4J<T> {
        this.prefix = prefix
        return this
    }

    /**
     * Sets the description of the command to build.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun withDescription(description: String): CommandBuilderD4J<T> {
        this.description = description
        return this
    }

    /**
     * Sets the usage of the command to build.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun withUsage(usage: String): CommandBuilderD4J<T> {
        this.usage = usage
        return this
    }

    /**
     * Sets whether the command to build should be executed along with its subcommand.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setExecWithSubcommands(execWithSubcommands: Boolean): CommandBuilderD4J<T> {
        this.execWithSubcommands = execWithSubcommands
        return this
    }


    /**
     * Sets the aliases of the command to build, taking in a vararg amount of strings.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun withAliases(vararg aliases: String): CommandBuilderD4J<T> {
        this.aliases = aliases.asList()
        return this
    }

    /**
     * Sets the aliases of the command to build, taking in a list of strings.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun withAliases(aliases: List<String>): CommandBuilderD4J<T> {
        this.aliases = aliases
        return this
    }

    /**
     * Sets whether the command to build is disabled.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setDisabled(disabled: Boolean): CommandBuilderD4J<T> {
        this.isDisabled = disabled
        return this
    }

    /**
     * Sets the permission levels of the command.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun withPerms(permissions: EnumSet<Permissions>): CommandBuilderD4J<T> {
        this.permissionLevels = permissions
        return this
    }

    /**
     * Sets whether the command requires a bot mention in order to activate.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setRequireMention(reqMention: Boolean): CommandBuilderD4J<T> {
        this.reqMention = reqMention
        return this
    }

    /**
     * Sets whether the command is allowed to be activated through a direct message to the bot.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setAllowDm(allowDm: Boolean): CommandBuilderD4J<T> {
        this.allowDm = allowDm
        return this
    }

    /**
     * Sets whether the command is forced to reply in a private chat or not.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setForceDmReply(forceDmReply: Boolean): CommandBuilderD4J<T> {
        this.forceDmReply = forceDmReply
        return this
    }

    /**
     * Sets whether the calling message is removed after activating the command.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setRemoveCallMsg(removeCallMsg: Boolean): CommandBuilderD4J<T> {
        this.removeCallMsg = removeCallMsg
        return this
    }

    /**
     * Sets the rate limit period time in milliseconds.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setRateLimitPeriod(rateLimitMs: Long): CommandBuilderD4J<T> {
        this.rateLimitMs = rateLimitMs
        return this
    }

    /**
     * Sets the tokens or maximum number of calls per rate limit period.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setTokenPerPeriod(tokensPerPeriod: Long): CommandBuilderD4J<T> {
        this.tokensPerPeriod = tokensPerPeriod
        return this
    }

    /**
     * Sets whether the command is rate limited on a guild-global level or on a per user basis.
     *
     * @return The current [CommandBuilderD4J] instance.
     */
    fun setRateLimitAsGuildGlobal(ratelimitOnGuild: Boolean): CommandBuilderD4J<T> {
        this.ratelimitOnGuild = ratelimitOnGuild
        return this
    }

    /**
     * Builds the Discord4J command with the provided [ICommandExecutableD4J].
     *
     * @return[CommandD4J] The built command.
     */
    fun build(executor: ICommandExecutableD4J<T>): CommandD4J<T> {
        if (aliases === null) aliases = Collections.singletonList(uniqueName)
        return object : CommandD4J<T>(CommandProperties(prefix, uniqueName, description, usage, execWithSubcommands, isDisabled, aliases!!),
                PermissionProperties(reqMention, allowDm, forceDmReply, removeCallMsg, permissionLevels, rateLimitMs, tokensPerPeriod, ratelimitOnGuild)) {
            @Throws(InvocationTargetException::class, IllegalAccessException::class)
            override fun executeWith(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder): T = executor.executeWith(context, event, msgBuilder)
        }
    }

    companion object {
        /**
         * Kotlin lambda helper to easily define the [executable][ICommandExecutableD4J] command method. It is recommended
         * to not use this in Java as a lambda can be directly used in [build].
         *
         * @return[ICommandExecutableD4J] The executable method for the command.
         */
        @JvmStatic
        fun <T> execute(handler: (CommandContext, MessageReceivedEvent, MessageBuilder) -> T): ICommandExecutableD4J<T> {
            return object : ICommandExecutableD4J<T> {
                override fun executeWith(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder): T = handler(context, event, msgBuilder)
            }
        }
    }
}