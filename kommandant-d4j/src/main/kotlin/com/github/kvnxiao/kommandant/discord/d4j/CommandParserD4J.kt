package com.github.kvnxiao.kommandant.discord.d4j

import com.github.kvnxiao.kommandant.command.CommandAnn
import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.command.CommandProperties
import com.github.kvnxiao.kommandant.command.ICommand
import com.github.kvnxiao.kommandant.discord.core.command.PermissionDefaults
import com.github.kvnxiao.kommandant.discord.core.command.PermissionLevel
import com.github.kvnxiao.kommandant.discord.core.command.PermissionProperties
import com.github.kvnxiao.kommandant.discord.core.command.Permissions
import com.github.kvnxiao.kommandant.impl.CommandParser
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.MessageBuilder
import java.lang.reflect.Method
import java.util.*

/**
 * The Discord4J implementation of a command parser. Parses a class containing the [CommandAnn] annotation and [PermissionLevel] annotation
 * to create Discord commands from these annotations.
 */
class CommandParserD4J : CommandParser() {

    /**
     * Creates a command by parsing a [CommandAnn] and [PermissionLevel] annotation, with its execution method set as the method
     * targeted by the annotation.
     *
     * @param[instance] The instance object for which its class is to be parsed.
     * @param[method] The method to invoke for command execution.
     * @param[annotation] The annotation to parse.
     * @return[ICommand] A newly created command with properties taken from the annotation.
     */
    override fun createCommand(instance: Any, method: Method, annotation: CommandAnn): ICommand<Any?> {
        // Get permissions from existing annotations
        if (method.isAnnotationPresent(PermissionLevel::class.java)) {
            val permissions: PermissionLevel = method.getAnnotation(PermissionLevel::class.java)
            val permissionLevel: EnumSet<Permissions> = PermissionDefaults.DEFAULT_PERMS_RW.clone()
            permissionLevel.addAll(permissions.level)

            return object : CommandD4J<Any?>(
                    CommandProperties(annotation.prefix, annotation.uniqueName, annotation.description, annotation.usage, annotation.execWithSubcommands, annotation.isDisabled, annotation.aliases.toList()),
                    PermissionProperties(permissions.reqMention, permissions.allowDm,  permissions.forceDmReply, permissions.removeCallMsg, permissionLevel, permissions.rateLimitPeriodMs, permissions.tokensPerPeriod, permissions.rateLimitOnGuild)) {
                override fun executeWith(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder): Any? {
                    return method.invoke(instance, context, event, msgBuilder)
                }
            }
        }

        // Default permissions
        return object : CommandD4J<Any?>(
                CommandProperties(annotation.prefix, annotation.uniqueName, annotation.description, annotation.usage, annotation.execWithSubcommands, annotation.isDisabled, annotation.aliases.toList()),
                PermissionProperties()) {
            override fun executeWith(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder): Any? {
                return method.invoke(instance, context, event, msgBuilder)
            }
        }
    }

}