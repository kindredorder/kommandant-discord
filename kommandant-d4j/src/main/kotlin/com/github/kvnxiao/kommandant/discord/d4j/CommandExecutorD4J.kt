package com.github.kvnxiao.kommandant.discord.d4j

import com.github.kvnxiao.kommandant.Kommandant.Companion.LOGGER
import com.github.kvnxiao.kommandant.command.CommandContext
import com.github.kvnxiao.kommandant.command.ICommand
import com.github.kvnxiao.kommandant.discord.core.command.Permissions
import com.github.kvnxiao.kommandant.discord.core.command.event.EventMissingPerms
import com.github.kvnxiao.kommandant.discord.core.command.event.EventRateLimited
import com.github.kvnxiao.kommandant.discord.d4j.utility.sendBuffered
import com.github.kvnxiao.kommandant.discord.d4j.utility.toPermissions
import com.github.kvnxiao.kommandant.impl.CommandExecutor
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageReceivedEvent
import sx.blah.discord.util.MessageBuilder
import java.util.*

/**
 * The Discord4J implementation of a command executor. Executes a specified command by running the [ICommandExecutableD4J] method.
 */
open class CommandExecutorD4J : CommandExecutor(), EventMissingPerms, EventRateLimited {

    /**
     * Checks whether or not the command can be executed. This compares the command's required permissions with a
     * Discord user's permissions, and additionally checks for rate limits.
     */
    override fun checkOtherSettings(command: ICommand<*>, context: CommandContext, vararg opt: Any?): Boolean {
        val discordCommand = command as CommandD4J<*>
        val event = opt[0] as MessageReceivedEvent
        val isDm = opt[1] as Boolean
        val hasBotMention = opt[2] as Boolean
        val deleteCmdMsg = command.perms.removeCallMsg as Boolean

        try {
            if (deleteCmdMsg) event.getMessage().delete()
        } catch (e: Exception) {
            // LOGGER.error(e.message)
        }

        return (hasPermissions(context, discordCommand, isDm, event) && commandNotRateLimited(context, discordCommand, event, isDm) && botMentionEquals(context, discordCommand, discordCommand.perms.reqMention, hasBotMention))
    }

    protected open fun commandNotRateLimited(context: CommandContext, command: CommandD4J<*>, event: MessageReceivedEvent, isDm: Boolean): Boolean {
        val guildId: String = if (event.guild != null && event.guild.id != null) event.guild.id else ""
        val isNotRateLimited: Boolean = if (!isDm) command.rateLimitManager.isNotRateLimited(guildId, event.author.id, command.perms) else command.rateLimitManager.isNotRateLimited(guildId, command.perms)

        if (!isNotRateLimited) onRateLimited(context, isDm, event)
        return isNotRateLimited
    }

    /**
     * Checks for the permissions of the user.
     */
    protected open fun hasPermissions(context: CommandContext, command: CommandD4J<*>, isDm: Boolean, event: MessageReceivedEvent): Boolean {
        if (isDm) {
            if (!command.perms.allowDm)
                LOGGER.debug("Executing command '$command' ignored because the command does now allow direct messages for activation.")
            return command.perms.allowDm
        }

        val reqPerms = command.perms.level
        val userPerms = event.message.channel.getModifiedPermissions(event.message.author).toPermissions()

        if (!userPerms.containsAll(reqPerms)) {
            reqPerms.removeAll(userPerms)
            onMissingPerms(context, reqPerms, event)
            return false
        }
        return true
    }

    /**
     * Checks whether the command requires a bot mention, and whether the message contained the bot mention.
     */
    protected open fun botMentionEquals(context: CommandContext, command: CommandD4J<*>, requireBotMention: Boolean, hasBotMention: Boolean): Boolean {
        if (requireBotMention == hasBotMention) return true
        if (requireBotMention) LOGGER.error("The command '$command' requires a mention to activate!")
        else LOGGER.error("The command '$command' cannot be activated with a mention!")
        return false
    }

    final override fun onMissingPerms(context: CommandContext, missingPerms: EnumSet<Permissions>, event: Any) = this.onMissingPerms(context, missingPerms, event as MessageReceivedEvent)

    final override fun onRateLimited(context: CommandContext, isDm: Boolean, event: Any) = this.onRateLimited(context, isDm, event as MessageReceivedEvent)

    /**
     * Default method implementation, where a user is messaged in private that they do not have the permission to
     * execute the command in a guild's channel. Missing permissions can never happen in a private channel.
     */
    open fun onMissingPerms(context: CommandContext, missingPerms: EnumSet<Permissions>, event: MessageReceivedEvent) {
        val user = event.message.author
        val privateChannel = event.client.getOrCreatePMChannel(user)

        LOGGER.debug("${event.message.author} can't execute command '${context.command}' in ${event.guild.name} - ${event.channel.name} due to missing permissions: $missingPerms")
        MessageBuilder(event.client).withChannel(privateChannel)
                .withContent("Sorry **${user.name}**, you do not have permission to execute the '${context.alias}' command in **${event.guild.name} - ${event.channel.name}** channel.")
                .sendBuffered()
    }

    /**
     * Default method implementation, where a user attempted to execute a command but has already hit the rate limit.
     */
    open fun onRateLimited(context: CommandContext, isDm: Boolean, event: MessageReceivedEvent) {
        val user = event.message.author
        val privateChannel = event.client.getOrCreatePMChannel(user)

        LOGGER.debug("${user.name} was rate-limited when attempting to execute command '${context.command}' ${if (isDm) "too fast." else "too fast in ${event.guild.name} - ${event.channel.name} channel."}")
        MessageBuilder(event.client).withChannel(privateChannel)
                .withContent("Slow down there **${user.name}**! You're trying to execute the '${context.alias}' command ${if (isDm) "too fast." else "too fast in **${event.guild.name} - ${event.channel.name}** channel."}")
                .sendBuffered()
    }
}
