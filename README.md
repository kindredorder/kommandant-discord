# Kommandant for Discord
[![](https://jitpack.io/v/org.bitbucket.KindredOrder/kommandant-discord.svg)](https://jitpack.io/#org.bitbucket.KindredOrder/kommandant-discord)
[![Apache License](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0)

A fast and modular command framework for Discord bots on the JVM.

[Documentation](https://kvnxiao.github.io/kommandant-discord/)

## Overview

**Kommandant for Discord** is a simple command framework library built for Discord bots targeting the JVM.
It uses [Kommandant](https://github.com/kvnxiao/Kommandant) under the hood as the base implementation for the command framework, which gives this library a high level of flexibility.

* [Features](#features)
* [Packages](#packages)
* [Usage - Discord4J](#usage---discord4j)
  * [Creating commands](#creating-commands)
    * [Builder](#using-a-builder)
    * [Annotations](#using-annotations)
  * [External command `.jar`s](#external-command-jar-loading)
  * [Sub-commands](#about-sub-commands)
* [Usage - JDA]() (WIP)
  
___

### Features

* Straightforward command creation using **annotations** and **builders**
* Simple **permissions** and command settings system for flexible command definitions (see [permissions](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-discord-core/src/main/kotlin/com/github/kvnxiao/kommandant/discord/core/PermissionDefaults.kt) and [command-properties]())
* **Load** and **save** command properties / permissions using a `.json` configuration file. Do this from startup, or even overwrite settings during runtime
* **Sub-commands** that can be executed as special "arguments" of parent commands
* **Modular commands**: Optionally package your commands into `.jar` files and load them dynamically on runtime! A "plug 'n play" solution that allows commands to be easily shared amongst users without needing to recompile their bots
* Useful utility functions (e.g. _sendBuffered_ for Discord4J to avoid rate-limits automatically; no need to litter your code with extra boilerplate)
* Fast, modular, and flexible, making custom implementations and extensions easy to do
* Use it in Kotlin *or* Java, it's up to you!

This library contains integration modules with JVM Discord libraries such as [Discord4J](https://github.com/austinv11/Discord4J) (and [JDA](https://github.com/DV8FromTheWorld/JDA) soon!).

Create your own commands or integrate the framework with your Discord bot by importing the library using your favourite dependency management tools!
Just replace `@VERSION@` with the latest release tag or use a commit hash.

## Packages

### Discord4J

#### Gradle

`build.gradle`

```gradle
allprojects {
  repositories {
    jcenter()
    maven { url 'https://jitpack.io' }
  }
}

dependencies {
        compile 'com.github.kvnxiao.kommandant-discord:kommandant-d4j:@VERSION@'
}
```

#### Maven

`pom.xml`

```xml
	<repositories>
		<repository>
		    <id>jitpack.io</id>
		    <url>https://jitpack.io</url>
		</repository>
	</repositories>

	<dependency>
	    <groupId>com.github.kvnxiao.kommandant-discord</groupId>
	    <artifactId>kommandant-d4j</artifactId>
	    <version>@VERSION@</version>
	</dependency>
```

## Usage - Discord4J

Use the `kommandant-d4j` module for bots and commands targeting Discord4J.

The `KommandantD4J` class is a `MessageReceivedEvent` listener and should be registered by the Discord4J client dispatcher if you are integrating this library with your bot on a source level. Otherwise, using the Discord4J module from [releases](https://github.com/kvnxiao/kommandant-discord/releases) and placing it in the `/modules/` folder of your bot should be enough to get it up and running.

### Creating Commands

Commands can be defined in one of three ways:
1. Using the `CommandD4J` constructor
2. Using the `CommandD4JBuilder` builder class
3. Using `@CommandAnn` and `@PermissionLevel` annotations in another public class.

All commands created must be registered to a `KommandantD4J` instance using `KommandantD4J#addCommand()` or `KommandantD4J#addAnnotatedCommands` for annotation-based commands.

Please check out the repo's [/kommandant-d4j/src/test/](https://github.com/kvnxiao/kommandant-discord/tree/master/kommandant-d4j/src/test) folder for more thorough examples!

#### Using a Builder

The command builder sets default command property values to avoid needing to type in verbose, explicit property values.
This compacts the declaration of a command, as it additionally supports lambda declarations for the execution method, so that explicit anonymous classes with overrided methods can be avoided.

Go here for a more thorough [Java example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/java/bot/JavaBotTest.java#L83), or go here for a more thorough [Kotlin example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/kotlin/bot/KotlinBotTest.kt#L66).

*Ping Command Example*
```kotlin
// This creates a ping command that can be called with '!ping' where the bot replies with 'pong!'
kommandant.addCommand(CommandBuilderD4J<Unit>("example_command")
	.withPrefix("!").withAliases("ping")
	.build(execute { commandContext, messageReceivedEvent, messageBuilder ->
    messageBuilder.withContent("pong!").sendBuffered()
    // Notice that the channel is not specified, this is because it is done by the framework automatically for you!
    // To send a private message, use CommandBuilderD4J#setForceDmReply(true)
}))
```

```java
// This creates a ping command that can be called with '!ping' where the bot replies with 'pong!'
kommandant.addCommand(new CommandBuilderD4J<Void>("example_command")
	.withPrefix("!").withAliases("ping")
	.build((commandContext, messageReceivedEvent, messageBuilder) -> {
	    sendBuffered(messageBuilder.withContent("pong!"));
	    // Notice that the channel is not specified, this is because it is done by the framework automatically for you!
	    // To send a private message, use CommandBuilderD4J#setForceDmReply(true)
	    return null;
	    // For a 'void' method, we must return null because Kommandant uses Java generics
	    // Defining a Void type lambda with return type of null is equivalent to that of a 'void' method
	})
);
```

#### Using Annotations

Commands defined using annotations in another class must have their class (or an instance of that class) registered in kommandant using:

[`Kommandant#addAnnotatedCommands(someClass)`](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/java/bot/JavaBotTest.java#L105)

Thorough examples of annotation-based command class examples can be found here:

 * Simple example: [Java example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/java/bot/JavaAnnotatedCommands.java) / [Kotlin example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/kotlin/bot/KotlinAnnotatedCommands.kt)
 * With `@PermissionLevel`s: [Java example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/java/bot/JavaPublicPrivateMessageCommands.java) / [Kotlin example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/kotlin/bot/KotlinPublicPrivateMessageCommands.kt)

*Admin Command Example*
```kotlin
open class AnnotationExample {
    // This creates an admin command requiring the user to have the ADMINISTRATOR permission in the guild channel to execute
    // The command can be called with '!admin'
    @CommandAnn(
            uniqueName = "annotation_example",
            aliases = arrayOf("admin"),
	    prefix = "!"
    )
    @PermissionLevel(
            level = arrayOf(Permissions.ADMINISTRATOR)
    )
    fun annotation(context: CommandContext, event: MessageReceivedEvent, msgBuilder: MessageBuilder) {
        msgBuilder.withContent("This is an admin command.").sendBuffered()
    }
}

// ... register the annotation example command
kommandant.addAnnotatedCommands(AnnotationExample::class)
```

```java
public class AnnotationExample {
    // This creates an admin command requiring the user to have the ADMINISTRATOR permission in the guild channel to execute
    // The command can be called with '!admin'
    @CommandAnn(
            uniqueName = "annotation_example",
            aliases = {"admin"},
	    prefix = "!"
    )
    @PermissionLevel(
            level = {Permissions.ADMINISTRATOR}
    )
    public void annotation(CommandContext context, MessageReceivedEvent event, MessageBuilder msgBuilder) {
        sendBuffered(msgBuilder.withContent("This is an admin command."));
    }
}

// ... register the annotation example command
kommandant.addAnnotatedCommands(AnnotationExample.class)
```

#### Using the Constructor

_NOT RECOMMENDED_ as it is very verbose, especially when using Java. Nevertheless, go [here for a Java example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/java/bot/JavaBotTest.java#L60), or go [here for a Kotlin example](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/kotlin/bot/KotlinBotTest.kt#L46).

___

### External command `.jar` loading

Commands can be packaged into their own .jar files and be loaded on runtime. This constitutes as a simple "plug 'n play" solution where commands can be swapped in and out, and shared amongst others without needing to recompile your bot.

Creating external command `.jar`s is simple.
Create a Maven or Gradle project / sub-module and use the `kommandant-d4j` module as a compileOnly dependency.
You may shade / shadow the jarfile to include other necessary transitive dependencies for your command but do not include `kommandant-d4j` itself in the fatjar!

#### For creating annotated commands:

Annotated command methods must be contained in a class implementing `IExternalContainer`. Kommandant will attempt to find all classes implementing the container interface and instantiate the entire class, registering them internally with `Kommandant#addAnnotatedCommands`.

#### For creating a single (complex) command:

For a single, large or complex command, one may want to extend `CommandD4J` (and therefore implementing and inheriting its constructors). Kommandant will attempt to find all of these subclasses and instantiate them, registering them internally with `Kommandant#addCommand`.

The `CommandD4J` subclass should have either a no-args constructor or a single parameter constructor relying on `CommandBank` is acceptable if the command requires manipulation and/or access to other commands that have been registered.

___

### About Sub-commands

A command may have subcommands specially assigned to a parent command.
Instead of sending arguments to a command and operating on the received arguments using some conditional logic, Kommandant can directly forward the execution process
to a registered subcommand if a user has the permission to activate that subcommand.

Go here for a builder-style subcommand example in [Java](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/java/bot/JavaBotTest.java#L109) or in [Kotlin](https://github.com/kvnxiao/kommandant-discord/blob/master/kommandant-d4j/src/test/java/bot/JavaBotTest.java#L109),
or check out the [annotation](#using-annotations) examples linked above for annotation-style subcommands.